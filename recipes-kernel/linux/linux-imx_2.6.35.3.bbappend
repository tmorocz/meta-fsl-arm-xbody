FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}-${PV}:"

PRINC := "${@int(PRINC) + 10}"


#include linux-imx-xbody.inc
#include linux-imx.inc

do_configure() {
	if [ -e ${S}/.config.fsl ]; then
		rm ${S}/.config.fsl
	fi
	cp ${S}/.config ${S}/.config.fsl

	echo "" > ${S}/.config
	CONF_SED_SCRIPT=""

	#
	# logo support, if you supply logo_linux_clut224.ppm in SRC_URI, then it's going to be used
	#
	if [ -e ${WORKDIR}/logo_linux_clut224.ppm ]; then
		install -m 0644 ${WORKDIR}/logo_linux_clut224.ppm drivers/video/logo/logo_linux_clut224.ppm
		kernel_conf_variable LOGO y
		kernel_conf_variable LOGO_LINUX_CLUT224 y
	fi

	# RTC configs only ds1307
	kernel_conf_variable RTC_DRV_DS1307 y
	kernel_conf_variable RTC_MC13892 n

	# AR1100 touchscreen configs
	kernel_conf_variable TOUCHSCREEN_EGALAX n
	kernel_conf_variable TOUCHSCREEN_MXC n
	kernel_conf_variable TOUCHSCREEN_DA9052 n
	kernel_conf_variable TOUCHSCREEN_MAX11801 n
	kernel_conf_variable TOUCHSCREEN_P1003 n
	kernel_conf_variable TOUCHSCREEN_AR1100SER y
	kernel_conf_variable SERIO y
	kernel_conf_variable SERIO_SERPORT y

	# fbcon modulba
	kernel_conf_variable FRAMEBUFFER_CONSOLE m

	# disable VGA output because it conflicts with ldb
	kernel_conf_variable FB_MXC_TVOUT_TVE n
	
	# ldb configs
	kernel_conf_variable FB_MXC_EPSON_VGA_SYNC_PANEL n
	kernel_conf_variable FB_MXC_TVOUT_TVE n
	kernel_conf_variable FB_MXC_SII902X n
	kernel_conf_variable FB_MXC_CH7026 n
	kernel_conf_variable FB_MXC_EINK_PANEL n

	# compat wireless pre conf
	kernel_conf_variable CFG80211 m
	kernel_conf_variable MAC80211 m

	sed -e "${CONF_SED_SCRIPT}" < '${S}/.config.fsl' >> '${S}/.config'
	
	kernel_do_configure
}

# Board specific patches
SRC_URI_append_imx53qsb-xbody = " 	file://uart.patch \
					file://rtc.patch \
					file://ar1100.patch \
					file://logo_linux_clut224.ppm \
					file://ldb.patch \
					"

