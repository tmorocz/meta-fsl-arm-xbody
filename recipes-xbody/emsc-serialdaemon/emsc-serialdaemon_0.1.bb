DESCRIPTION = "The canonical example of init scripts"
SECTION = "base"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${WORKDIR}/LICENSE;md5=f1f0d5f2df4f435a91702726ad65bec7"
RDEPENDS_${PN} = "initscripts"
PR = "r2"

SRC_URI = "file://emsc-serialdaemon \
	   file://emsc-serialdaemon.c \
	   file://LICENSE \
	   "

CONFFILES_${PN} += "${sysconfdir}/init.d/emsc-serialdaemon"

do_compile () {
	${CC} ${WORKDIR}/emsc-serialdaemon.c -L. -lutil -o ${WORKDIR}/emsc-tcpseriald
}

inherit update-rc.d

INITSCRIPT_NAME = "emsc-serialdaemon"
INITSCRIPT_PARAMS = "defaults 20"

do_install() {
	install -d ${D}${sysconfdir} \
	           ${D}${sysconfdir}/init.d
	install -m 0755 ${WORKDIR}/${INITSCRIPT_NAME} ${D}${sysconfdir}/init.d
        cat ${WORKDIR}/${INITSCRIPT_NAME} | \
            sed -e 's,/etc,${sysconfdir},g' \
                -e 's,/usr/sbin,${sbindir},g' \
                -e 's,/var,${localstatedir},g' \
                -e 's,/usr/bin,${bindir},g' \
                -e 's,/usr,${prefix},g' > ${D}${sysconfdir}/init.d/${INITSCRIPT_NAME}
        chmod 755 ${D}${sysconfdir}/init.d/${INITSCRIPT_NAME}

	install -d ${D}${sbindir}
	install -m 0755 ${WORKDIR}/emsc-tcpseriald ${D}${sbindir}/
}

