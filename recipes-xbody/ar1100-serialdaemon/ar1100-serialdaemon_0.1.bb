DESCRIPTION = "The canonical example of init scripts"
SECTION = "base"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${WORKDIR}/license.txt;md5=b234ee4d69f5fce4486a80fdaf4a4263"
RDEPENDS_${PN} = "initscripts"
PR = "r3"

SRC_URI = "file://ar1100-serialdaemon \
	   file://ar1100-serialdaemon.c \
	   file://serio-mchp.h \
	   file://license.txt \
	   file://10-ar1xxxuart.rules \
	   file://pointercal \
	   "

CONFFILES_${PN} += "${sysconfdir}/init.d/ar1100-serialdaemon"

do_compile () {
	${CC} ${WORKDIR}/ar1100-serialdaemon.c -L. -lutil -o ${WORKDIR}/ar1100-seriald
}

inherit update-rc.d

INITSCRIPT_NAME = "ar1100-serialdaemon"
INITSCRIPT_PARAMS = "defaults 20"

do_install() {
	install -d ${D}${sysconfdir} \
	           ${D}${sysconfdir}/init.d
	install -m 0755 ${WORKDIR}/${INITSCRIPT_NAME} ${D}${sysconfdir}/init.d
        cat ${WORKDIR}/${INITSCRIPT_NAME} | \
            sed -e 's,/etc,${sysconfdir},g' \
                -e 's,/usr/sbin,${sbindir},g' \
                -e 's,/var,${localstatedir},g' \
                -e 's,/usr/bin,${bindir},g' \
                -e 's,/usr,${prefix},g' > ${D}${sysconfdir}/init.d/${INITSCRIPT_NAME}
        chmod 755 ${D}${sysconfdir}/init.d/${INITSCRIPT_NAME}

	install -d ${D}${sbindir}
	install -m 0755 ${WORKDIR}/ar1100-seriald ${D}${sbindir}/

	#udev rules install
	if [ -e "${WORKDIR}/10-ar1xxxuart.rules" ]; then
		install -d ${D}${sysconfdir}/udev/rules.d
		install -m 0644 ${WORKDIR}/10-ar1xxxuart.rules ${D}${sysconfdir}/udev/rules.d
	fi
	
	#default pointercal install
	if [ -e "${WORKDIR}/pointercal" ]; then
		install -d ${D}${sysconfdir}
		install -m 0644 ${WORKDIR}/pointercal ${D}${sysconfdir}
	fi
	
	#tslib_vars.sh install
	if [ -e "${WORKDIR}/tslib_vars.sh" ]; then
		install -d ${D}${sysconfdir}/profile.d
		install -m 0644 ${WORKDIR}/tslib_vars.sh ${D}${sysconfdir}/profile.d
	fi
}

