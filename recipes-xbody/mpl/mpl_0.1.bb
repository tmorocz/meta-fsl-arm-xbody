DESCRIPTION = "A simple video player"
SECTION = "base"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${WORKDIR}/license.txt;md5=b234ee4d69f5fce4486a80fdaf4a4263"
PR = "r2"

SRC_URI = "file://mpl-0.1.tar.gz \
	    file://license.txt"

DEPENDS = "qt4-embedded"
inherit qt4e pkgconfig


do_install() {
	install -d ${D}${bindir}
	install -m 0755 ${S}/${BPN} ${D}${bindir}
#	oe_runmake INSTALL_ROOT=${D} install
}
