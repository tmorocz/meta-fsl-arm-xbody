DESCRIPTION = "xbody updater script"
SECTION = "base"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${WORKDIR}/license.txt;md5=b234ee4d69f5fce4486a80fdaf4a4263"
RDEPENDS_${PN} = "initscripts"
PR = "r1"

SRC_URI = "file://xbody-updater \
	    file://license.txt \
	   "

inherit update-rc.d

INITSCRIPT_NAME = "xbody-updater"
INITSCRIPT_PARAMS = "start 30 5 ."

do_install() {
	install -d ${D}${sysconfdir} \
	           ${D}${sysconfdir}/init.d
	install -m 0755 ${WORKDIR}/${INITSCRIPT_NAME} ${D}${sysconfdir}/init.d
}

