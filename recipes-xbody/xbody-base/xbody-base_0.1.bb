DESCRIPTION = "xbody base scripts"
SECTION = "base"
LICENSE = "GPLv2"
LIC_FILES_CHKSUM = "file://${WORKDIR}/license.txt;md5=b234ee4d69f5fce4486a80fdaf4a4263"
RDEPENDS_${PN} = "initscripts"
PR = "r1"

SRC_URI = "file://xbody-start \
	    file://xbody.sh \
	    file://license.txt \
	   "

inherit update-rc.d

INITSCRIPT_NAME = "xbody-start"
INITSCRIPT_PARAMS = "start 40 5 ."

do_install() {
	install -d ${D}${sysconfdir} \
	           ${D}${sysconfdir}/init.d
	install -m 0755 ${WORKDIR}/${INITSCRIPT_NAME} ${D}${sysconfdir}/init.d

	install -d ${D}/opt \
	           ${D}/opt/xbody
	install -m 0755 ${WORKDIR}/xbody.sh ${D}/opt/xbody
}

