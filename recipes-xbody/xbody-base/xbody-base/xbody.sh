#!/bin/sh
cd /opt/xbody

#./tcpseriald -serial /dev/ttymxc3 -baud 115200 -port 1235 &

./xbody_control >/dev/null 2>&1 &
#./xbody_control &


export TSLIB_CONSOLEDEVICE=none
export TSLIB_FBDEVICE=/dev/fb0
export TSLIB_TSDEVICE=/dev/input/touchscreen0
export TSLIB_PLUGINDIR=/usr/lib/ts
export TSLIB_CONFFILE=/etc/ts.conf
export TSLIB_CALIBFILE=/etc/pointercal
export QWS_MOUSE_PROTO="tslib:/dev/input/touchscreen0"

./xbody -qws >/dev/null 2>&1
#./xbody -qws

killall xbody_control
#killall tcpseriald

poweroff
