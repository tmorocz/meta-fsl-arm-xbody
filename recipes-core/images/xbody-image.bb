DESCRIPTION = "An image that will launch into the demo application for the embedded (not based on X11) version of Qt."
LICENSE = "MIT"
PR = "r3"

LIC_FILES_CHKSUM = "file://${COREBASE}/LICENSE;md5=3f40d7994397109285ec7b81fdeb3b58 \
                    file://${COREBASE}/meta/COPYING.MIT;md5=3da9cfbcb788c80a0384361b4de20420"

IMAGE_INSTALL += "\
	${CORE_IMAGE_BASE_INSTALL} \
	perl mpl \
	emsc-serialdaemon ar1100-serialdaemon \
	emsc-poweroff xbody-updater xbody-base \
	compat-wireless-all linux-firmware \
	fsl-mm-codeclib \
	gnupg gpgme \
	gst-fsl-plugin gst-plugins-good-meta gst-plugins-bad-meta gst-plugins-base-meta \
	imx-test libz160 \
	mc minicom python-gst screen setserial subversion unzip zip wget python-dbus \
	packagegroup-base \
	packagegroup-core-boot \
	packagegroup-core-gtk-directfb \
	packagegroup-core-qt4e \
	packagegroup-core-ssh-openssh \
	iptables \
"

IMAGE_FEATURES += "package-management"

inherit core-image

